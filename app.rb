require 'dry-struct'
require "awesome_print"
AwesomePrint.defaults = {
  indent: 2,
  index:  false,
}

require "json"
require 'securerandom'

require "./models/types"
require "./models/url"

require "./repositories/url_repository"

class App < Roda
  use Rack::Session::Cookie, secret: ENV.fetch('RACK_SECRET')

  plugin :empty_root
  plugin :render, engine: 'slim'
  plugin :csrf, raise: true
  plugin :basic_auth, authenticator: proc { |user, pass|
    user == pass
  },
         realm: 'Restricted Area' # default
  route do |r|

    puts "  >>>>> #{__FILE__}:#{__LINE__}".purple
    params = process_params(r.params)
    ap params
    puts "  >>>>> #{__FILE__}:#{__LINE__}".purple

    repo = UrlRepository.new

    r.root do
      '<h1>URL Shortener</h1>'
    end

    r.on 'manage' do
      r.basic_auth
      r.root do
        view 'new', locals: {
               urls: repo.all
             }
      end

      r.post do

        r.is '' do
          repo.add(repo.new_url(**params))
          r.redirect '/manage'
        end

        r.is "delete", :slug do |slug|
          repo.destroy(slug)
          r.redirect '/manage'
        end

      end
    end

    r.is :slug do |slug|
      url = repo.find(slug)

      url.track!(env)

      r.redirect url.destination
    end
  end

  def process_params(original_params)
    original_params.dup.tap { |params|
      params
        .transform_keys! { |k| k.to_sym }
        .delete_if { |k, v| v.to_s =~ /\A\s*\Z/}
      params.delete(:_csrf)
    }

  end
end
