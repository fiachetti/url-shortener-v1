class UrlRepository
  URLS = [
    Url.for(slug: 'duck',   destination: 'http://duckduckgo.com',  count: rand(10)),
    Url.for(slug: "yahoo",  destination: 'http://yahoo.com',       count: rand(10)),
    Url.for(slug: 'google', destination: 'http://google.com',      count: rand(10)),
    Url.for(slug: "ex",     destination: 'http://example.com',     count: rand(10)),
  ]

  def find(slug)
    URLS.detect { |url| url.slug == slug}
  end

  def all
    URLS.sort_by { |url| [url.custom? ? 0 : 1, -url.count] }
  end

  def add(url)
    URLS << url
  end

  def destroy(slug)
    URLS.delete_if { |url| url.slug == slug }
  end

  def slugs
    URLS.map(&:slug)
  end

  def new_url(slug: nil, destination:)
    Url.for(slug: slug, destination: destination)
  end
end
