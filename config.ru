if ENV['RACK_ENV'] != 'production'
  require "dotenv"
  Dotenv.load
end

require "roda"

require "./app"

run App
