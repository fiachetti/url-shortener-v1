class Url < Dry::Struct
  attribute :slug,        Types::Strict::String
  attribute :destination, Types::Strict::String
  attribute :count,       Types::Strict::Integer.default(0)
  attribute :data_json,   Types::Strict::String.optional.default("{}")
  attribute :custom,      Types::Strict::Bool.default(false)

  def self.for(slug: nil, destination:, count: 0, **rest)
    custom = !! slug
    slug   = slug || SecureRandom.urlsafe_base64(3).downcase
      
    new(
      slug:        slug,
      destination: destination,
      count:       count,
      custom:      custom,
    )
  end

  def track!(params)
    puts "tracking".green
    ap params
    self.attributes[:count] += 1
  end

  def data
    parsed = JSON.parse(attributes[:data_json].to_s)
  end

  def custom?
    !! attributes[:custom]
  end
end
